pyTuple
=======
The function pyTuple creates a virtual Python object
of type PythonInR_Tuple.    

If no value is provided a virtual Python tuple for an existing
Python object is created. If the value is NULL an empty
virtual Python object for an empty tuple is created.
If the value is a vector or tuple a new Python
object based on the vector or list is created.    


Usage
-----
.. code-block:: R

    pyTuple(key, value, regFinalizer = FALSE)

Arguments
---------
.. raw:: html

    <table style="width:100%">
      <tr>
        <td style="background-color:#FFFFE0;"><b>key</b> &nbsp;</td>
        <td style="background-color:#E0E6F8;">a character string giving the name of the Python object.</td>
      </tr>
      <tr>
        <td style="background-color:#FFFFE0;"><b>value</b> &nbsp;</td>
        <td style="background-color:#E0E6F8;">an optional value, allowed values are vectors, lists and NULL.</td>
      </tr>
      <tr>
        <td style="background-color:#FFFFE0;"><b>regFinalizer</b> &nbsp;</td>
        <td style="background-color:#E0E6F8;">a logical indicating if a finalizer should be
    be registered, the default value is TRUE.</td>
      </tr>
    </table>

Examples
--------
.. code-block:: R

    if ( pyIsConnected() ){
    pyExec('myPyTuple = (1, 2, 5, "Hello R!")')
    # create a virtual Python tuple for an existing tuple
    myTuple <- pyTuple("myPyTuple")
    myTuple[0]
    tryCatch({myTuple[1] <- "should give an error since tuple are not mutable"},
             error = function(e) print(e))
    myTuple
    # create a new Python tuple and virtual tuple
    newTuple <- pyTuple('myNewTuple', list(1:3, 'Hello Python'))
    newTuple[1]
    }
