Type Casting
============

(under construction (last change 17.05.2015))

R to Python (pySet)
-------------------

+----------------------+--------------+-----------+
| R                    | length (n)   | Python    |
+======================+==============+===========+
| NULL                 |              | None      |
+----------------------+--------------+-----------+
| any type             | 0            | None      |
+----------------------+--------------+-----------+
| logical              | 1            | boolean   |
+----------------------+--------------+-----------+
| integer              | 1            | integer   |
+----------------------+--------------+-----------+
| numeric              | 1            | double    |
+----------------------+--------------+-----------+
| character            | 1            | unicode   |
+----------------------+--------------+-----------+
| logical              | n > 1        | list      |
+----------------------+--------------+-----------+
| integer              | n > 1        | list      |
+----------------------+--------------+-----------+
| numeric              | n > 1        | list      |
+----------------------+--------------+-----------+
| character            | n > 1        | list      |
+----------------------+--------------+-----------+
| list without names   | n > 0        | list      |
+----------------------+--------------+-----------+
| list with names      | n > 0        | dict      |
+----------------------+--------------+-----------+
| matrix               | n > 0        | dict      |
+----------------------+--------------+-----------+
| data.frame           | n > 0        | dict      |
+----------------------+--------------+-----------+

| **NOTE (named lists):**
| When executing ``pySet("x", list(b=3, a=2))`` and ``pyGet("x")`` the
order of the elements in x will change. This is not a special behavior
of **PythonInR** but the default behavior of Python.

| **NOTE (matrix):**
| Matrices are either transformed to an dictionary with the keys
| - *matrix* the matrix stored as list of lists
| - *rownames* the row names of the matrix
| - *colnames* the column names of the matrix
| - *dim* the dimension of the matrix
| or to an numpy array (if the option useNumpy is set to TRUE).

| **NOTE (data.frame):**
| Data frames are either transformed to an dictionary with the keys
| - *data.frame* the data.frame stored as a dictionary
| - *rownames* the rownames of the data.frame
| - *dim* the dimension of the data.frame
| of to an pandas data.frame (if the option usePandas is set to TRUE).

R to Python (pyGet)
-------------------

+-----------+------------------------+----------------+
| Python    | R                      | simplify       |
+===========+========================+================+
| None      | NULL                   | TRUE / FALSE   |
+-----------+------------------------+----------------+
| boolean   | logical                | TRUE / FALSE   |
+-----------+------------------------+----------------+
| integer   | integer                | TRUE / FALSE   |
+-----------+------------------------+----------------+
| double    | numeric                | TRUE / FALSE   |
+-----------+------------------------+----------------+
| string    | character              | TRUE / FALSE   |
+-----------+------------------------+----------------+
| unicode   | character              | TRUE / FALSE   |
+-----------+------------------------+----------------+
| bytes     | character              | TRUE / FALSE   |
+-----------+------------------------+----------------+
| list      | list                   | FALSE          |
+-----------+------------------------+----------------+
| list      | list or vector         | TRUE           |
+-----------+------------------------+----------------+
| dict      | named list             | FALSE          |
+-----------+------------------------+----------------+
| dict      | named list or vector   | TRUE           |
+-----------+------------------------+----------------+

| **NOTE (bytes):**
| In short, in Python 3 the data type string was replaced by the data
type bytes. More information can be found
`here <http://www.diveintopython3.net/strings.html>`__.
